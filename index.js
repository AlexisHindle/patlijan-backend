const express = require('express');
const cors = require('cors');
const os = require('os');
const app = express();
app.use(express.json());
app.use(express.static("."));

let stripe;
// Look at this link for how to set keys
// These need to be changed for life data - currently in test
const keyPublishable = process.env.PUBLISHABLE_KEY;
// const keySecret = process.env.SECRET_KEY;   
const keySecret = 	
"sk_test_51H0angLuOK3oWcoUtOoPI3zX8Vq4UppYngIT2NuFMGoEiQV5OmFBioxCvzyrnXc1uEznA5pVe7IlPihl2JYHz0et00GhbnpIHZ";   
stripe = require('stripe')(keySecret);
// hard coded configuration object
conf = {
    // look for PORT environment variable,
    // else look for CLI argument,
    // else use hard coded value for port 8080
    port: process.env.PORT || process.argv[2] || 8080,
    // origin undefined handler
    // see https://github.com/expressjs/cors/issues/71
    originUndefined: function (req, res, next) {
        if (!req.headers.origin) {
            res.json({
                mess: 'Hi you are visiting the service locally. If this was a CORS the origin header should not be undefined'
            });
        } else {
            next();
        }
    },
    // Cross Origin Resource Sharing Options
    cors: {
        // origin handler
        origin: function (origin, cb) {
            // setup a white list
            let wl = ['http://localhost:4200', 'http://patlijan.co.uk', 'http://www.patlijan.co.uk', 'https://patlijan.co.uk', 'https://www.patlijan.co.uk'];
            if (wl.indexOf(origin) != -1) {
                cb(null, true);
            } else {
                cb(new Error('invalid origin: ' + origin), false);
            }
        },
        optionsSuccessStatus: 200
    }
};

// use origin undefined handler, then cors for all paths
app.use(conf.originUndefined, cors(conf.cors));

// get at root
app.get('/', function (req, res, next) {
    res.json({
        mess: 'hello it looks like you are on the whitelist',
        origin: req.headers.origin,
        os_hostname: os.hostname(),
        os_cpus: os.cpus()
    });
});

app.post("/create-payment-intent", async (req, res) => {
    const paymentIntent = await stripe.paymentIntents.create({
        amount: req.body.amount,
        currency: 'gbp',
        payment_method_types: ['card'],
        receipt_email: req.body.email,
        description: 'Patlijan Food Order'
        });
      res.send(paymentIntent)
});
 
app.listen(conf.port, function () {
    console.log('CORS-enabled JSON service is live on port: ' + conf.port);
});